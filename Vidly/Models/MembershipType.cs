﻿namespace Vidly.Models
{
    public enum MembershipOption : byte
    {
        Unknown = 0,
        PayAsYouGo = 1,
        Monthly = 2,
        Quarterly = 3,
        Yearly = 4
    }

    public class MembershipType
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public short SignUpFee { get; set; }
        public byte DurationInMonths { get; set; }
        public byte DiscountRate { get; set; }
    }
}