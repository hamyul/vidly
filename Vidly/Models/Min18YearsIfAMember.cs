﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public sealed class Min18YearsIfAMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (Customer)validationContext.ObjectInstance;

            if (customer.MembershipTypeId.Equals(MembershipOption.Unknown) ||
                customer.MembershipTypeId.Equals(MembershipOption.PayAsYouGo))
                return ValidationResult.Success;

            if (customer.Birthdate == null)
                return new ValidationResult("Customer's birthdate is required.");

            var age = DateTime.Today.Year - customer.Birthdate.Value.Year;

            return (age >= 18)
                ? ValidationResult.Success
                : new ValidationResult("Customer should be at least 18 years old to go on a membership.");
        }
    }
}