﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Dtos
{
    public class MovieDto
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [Required]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the genre identifier.
        /// </summary>
        [Required]
        public int GenreId { get; set; }

        /// <summary>
        /// Gets or sets the release date.
        /// </summary>
        [Required]
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Gets or sets the date added.
        /// </summary>
        [Required]
        public DateTime DateAdded { get; set; }

        /// <summary>
        /// Gets or sets the number in stock.
        /// </summary>
        [Required]
        [Range(1, 20, ErrorMessage = "The field 'Number in Stock' must be between 1 and 20.")]
        public int NumberInStock { get; set; }
    }
}