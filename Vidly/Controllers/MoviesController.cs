﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    /// <summary>
    /// The Movie controller class.
    /// </summary>
    /// <seealso cref="System.Web.Mvc.Controller" />
    public class MoviesController : Controller
    {
        /// <summary>
        /// The context.
        /// </summary>
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoviesController"/> class.
        /// </summary>
        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        /// <summary>
        /// Performs a GET to the Details page.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The action result to be handled by the MVC Framework.</returns>
        public ActionResult Details(int id)
        {
            var movie = _context.Movies.Include(m => m.Genre).FirstOrDefault(a => a.Id.Equals(id));
            return View(movie);
        }

        /// <summary>
        /// Edits the the movie with the specified identifier.
        /// </summary>
        /// <param name="id">The movie identifier.</param>
        /// <returns>The action result to be handled by the MVC Framework.</returns>
        public ActionResult Edit(int id)
        {
            var movie = _context.Movies.Include(m => m.Genre).SingleOrDefault(a => a.Id == id);

            if (movie == null)
                return HttpNotFound();

            var viewModel = new MovieFormViewModel(movie)
            {
                Genres = _context.Genres.ToList()
            };

            return View("MovieForm", viewModel);
        }

        // GET: Movie
        /// <summary>
        /// Performs a GET to the Index page.
        /// </summary>
        /// <returns>The action result to be handled by the MVC Framework.</returns>
        public ActionResult Index()
        {
            var movies = _context.Movies.Include(m => m.Genre).ToList();
            return View(movies);
        }

        /// <summary>
        /// Performs a GET to the Movie creation page.
        /// </summary>
        /// <returns>The action result to be handled by the MVC Framework.</returns>
        public ActionResult New()
        {
            var genres = _context.Genres.ToList();
            var viewModel = new MovieFormViewModel
            {
                Genres = genres
            };

            return View("MovieForm", viewModel);
        }

        /// <summary>
        /// Saves the specified movie.
        /// </summary>
        /// <param name="movie">The movie.</param>
        /// <returns>The action result to be handled by the MVC Framework.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Movie movie)
        {
            // If the model state is not valid, redirects it to the MovieForm view.
            if (!ModelState.IsValid)
            {
                var viewModel = new MovieFormViewModel(movie)
                {
                    Genres = _context.Genres.ToList()
                };
                return View("MovieForm", viewModel);
            }

            if (movie.Id == 0)
            {
                movie.DateAdded = DateTime.Now;
                _context.Movies.Add(movie);
            }
            else
            {
                var movieInDb = _context.Movies.Single(a => a.Id == movie.Id);
                movieInDb.Title = movie.Title;
                movieInDb.GenreId = movie.GenreId;
                movieInDb.ReleaseDate = movie.ReleaseDate;
                movieInDb.DateAdded = movie.DateAdded;
                movieInDb.NumberInStock = movie.NumberInStock;
            }

            _context.SaveChanges();
            return RedirectToAction(nameof(Index), "Movies");
        }
    }
}