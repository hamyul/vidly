﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Vidly.Dtos;
using Vidly.Models;

namespace Vidly.Controllers.api
{
    public class MoviesController : ApiController
    {
        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        // GET api/movies/1
        public MovieDto GetMovie(int id)
        {
            var movie = _context.Movies.FirstOrDefault(m => m.Id == id);
            if (movie == null)
                NotFound();

            return Mapper.Map<Movie, MovieDto>(movie);
        }

        // GET api/movies
        public IEnumerable<MovieDto> GetMovie()
        {
            var movies = _context.Movies.ToList();
            return Mapper.Map<IEnumerable<Movie>, IEnumerable<MovieDto>>(movies);
        }

        // POST api/movies
        public MovieDto CreateMovie(MovieDto movieDto)
        {
            if (movieDto == null)
                BadRequest();

            var movie = Mapper.Map<MovieDto, Movie>(movieDto);
            _context.Movies.Add(movie);
            _context.SaveChanges();

            return Mapper.Map<Movie, MovieDto>(movie);
        }

        // PUT api/movies/1
        public MovieDto UpdateMovie(int id, MovieDto movieDto)
        {
            if (movieDto == null)
                BadRequest();

            var movie = _context.Movies.FirstOrDefault(m => m.Id == id);
            if (movie == null)
                NotFound();

            Mapper.Map<MovieDto, Movie>(movieDto, movie);
            _context.SaveChanges();

            return movieDto;
        }

        // DELETE api/movies/1
        public void DeleteMovie(int id)
        {
            var movie = _context.Movies.FirstOrDefault(m => m.Id == id);
            if (movie == null)
                NotFound();

            _context.Movies.Remove(movie);
            _context.SaveChanges();
        }
    }
}