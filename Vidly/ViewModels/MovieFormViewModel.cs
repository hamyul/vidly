﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Vidly.Models;

namespace Vidly.ViewModels
{
    /// <summary>
    /// The movie view model
    /// </summary>
    public class MovieFormViewModel
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="MovieFormViewModel"/> class.
        /// </summary>
        public MovieFormViewModel()
        {
            Id = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MovieFormViewModel"/> class.
        /// </summary>
        /// <param name="movie">The movie.</param>
        public MovieFormViewModel(Movie movie)
        {
            Id = movie.Id;
            Title = movie.Title;
            GenreId = movie.GenreId;
            ReleaseDate = movie.ReleaseDate;
            NumberInStock = movie.NumberInStock;
        }

        /// <summary>
        /// Gets or sets the genre identifier.
        /// </summary>
        [Required]
        public int? GenreId { get; set; }

        /// <summary>
        /// Gets or sets the genres.
        /// </summary>
        public IEnumerable<Genre> Genres { get; set; }
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Gets or sets the number in stock.
        /// </summary>
        [Display(Name = "Number in Stock")]
        [Required]
        [Range(1, 20, ErrorMessage = "The field 'Number in Stock' must be between 1 and 20.")]
        public int? NumberInStock { get; set; }

        /// <summary>
        /// Gets or sets the release date.
        /// </summary>
        [Required]
        public DateTime? ReleaseDate { get; set; }
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [Required]
        public string Title { get; set; }
    }
}